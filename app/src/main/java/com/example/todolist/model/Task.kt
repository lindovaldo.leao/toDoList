package com.example.todolist.model

class Task(
    val urgent:Boolean,
    val text:String,
    var done:Boolean = false,
    var id:Long = 0
) {

    override fun toString(): String {
        return "(${if(!this.urgent) "Not " else ""} Urgent) ${this.text}\n"
    }

}