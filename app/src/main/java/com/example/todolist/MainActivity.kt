package com.example.todolist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.example.todolist.data.DAOTaskSingleton
import com.example.todolist.model.Task

class MainActivity : AppCompatActivity() {

    private lateinit var txtTodoList:TextView
    private lateinit var swUrgent:SwitchCompat
    private lateinit var etxTask:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.txtTodoList = findViewById(R.id.txtTodoList)
        this.swUrgent = findViewById(R.id.swUrgent)
        this.etxTask = findViewById(R.id.etxTask)
    }

    fun onClickInsert(v:View){

        val text = this.etxTask.text.toString()

        if(text.isNotBlank()){

            val urgent = this.swUrgent.isChecked
            val t = Task(urgent, text)
            DAOTaskSingleton.add(t)

            this.txtTodoList.append(t.toString())
            this.etxTask.text.clear()
            this.swUrgent.isChecked = false

        }

    }
}