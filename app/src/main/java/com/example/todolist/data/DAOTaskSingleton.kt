package com.example.todolist.data

import com.example.todolist.model.Task

object DAOTaskSingleton {

    private var serial:Long = 0
    private val tasks = ArrayList<Task>()

    fun add(t:Task){
        this.tasks.add(t)
        t.id = serial++
    }

    fun getTasks():ArrayList<Task>{
        return this.tasks
    }


}